package com.proyectoF.proyectfinal;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Usuario;
import com.proyectoF.proyectfinal.WebService.WebService;

import org.json.JSONObject;

public class ContactoActivity extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {
    private Usuario usuario = LoginActivity.USUARIO;
    private EditText eTTel;
    private EditText eTMovil;
    private EditText eTCorreo;
    private EditText eTCorreoAlterno;
    private Button btnActualizar;
    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_contacto, container, false);
        requestQueue = Volley.newRequestQueue(getActivity());

        eTTel = (EditText) view.findViewById(R.id.Contacto_EditTxt_Telefono);
        eTMovil = (EditText) view.findViewById(R.id.Contacto_EditTxt_Movil);
        eTCorreo = (EditText) view.findViewById(R.id.Contacto_EditTxt_Correo);
        eTCorreoAlterno = (EditText)view.findViewById(R.id.Contacto_EditTxt_CorreoAlter);
        btnActualizar = (Button) view.findViewById(R.id.Contacto_btn_ActualizarDatos);

        eTTel.setText(usuario.getTelefono());
        eTMovil.setText(usuario.getTelefonoMovil());
        eTCorreo.setText(usuario.getCorreo());
        eTCorreoAlterno.setText(usuario.getCorreoAlterno());

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnActualizar.setEnabled(false);
                wsActualizar(eTTel.getText().toString(),eTMovil.getText().toString(),eTCorreo.getText().toString(),eTCorreoAlterno.getText().toString());
                btnActualizar.setEnabled(true);
            }
        });
        return view;
    }



    public void wsActualizar(String tel, String telMovil, String correo, String correo2) {
        String url = WebService.WSACTUALIZAR +
                "?telefono=" + tel +
                "&movil=" + telMovil +
                "&correo=" + correo +
                "&correoalterno=" + correo2 +
                "&id=" + usuario.getId();
        LoginActivity.USUARIO.setTelefono(tel);
        LoginActivity.USUARIO.setTelefonoMovil(telMovil);
        LoginActivity.USUARIO.setCorreo(correo);
        LoginActivity.USUARIO.setCorreoAlterno(correo2);

        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
        Log.e("url",url);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getActivity(),"Datos actualizados",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getActivity(),"Datos actualizados",Toast.LENGTH_SHORT).show();
    }
}