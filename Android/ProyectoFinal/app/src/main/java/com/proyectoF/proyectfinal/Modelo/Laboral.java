package com.proyectoF.proyectfinal.Modelo;

public class Laboral
{
    private int id;
    private String nombreEmpresa;
    private String puesto;
    private String fechaIngreso;
    private int usuario_id;
    private int status;

    public Laboral()
    {
        this.setId(0);
        this.setNombreEmpresa("");
        this.setPuesto("");
        this.setFechaIngreso("");
        this.setUsuario_id(0);
        this.setStatus(0);
    }

    public Laboral(int id, String nombreEmpresa, String puesto, String fechaIngreso, int usuario_id, int status)
    {
        this.setId(id);
        this.setNombreEmpresa(nombreEmpresa);
        this.setPuesto(puesto);
        this.setFechaIngreso(fechaIngreso);
        this.setUsuario_id(usuario_id);
        this.setStatus(status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
