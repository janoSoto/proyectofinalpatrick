package com.proyectoF.proyectfinal.Modelo.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.proyectoF.proyectfinal.LoginActivity;

public class bd_validationQuery  extends SQLiteOpenHelper {
    public bd_validationQuery(Context context){
        super(context,BD_Validation.DATABASE_NAME,null,BD_Validation.DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BD_Validation.LOGIN_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(BD_Validation.LOGIN_TABLE_DROP);
        this.onCreate(db);
    }

    public void insert(){
        SQLiteDatabase db=this.getReadableDatabase();
        db.isOpen();
        ContentValues values=new ContentValues();
        values.put(BD_Validation.VALIDATION_LOGIN.ID_COL,"1");//nombre
        values.put(BD_Validation.VALIDATION_LOGIN.ID_USER,"");//nombre
        values.put(BD_Validation.VALIDATION_LOGIN.status,"0");//nombre

        db.insert(BD_Validation.VALIDATION_LOGIN.TABLE_NAME,null,values);
        // 4. Cerramos la conexión comn la BD
        db.close();

    }
    public void update(int status,int id_user) {
        SQLiteDatabase db=this.getWritableDatabase();
        db.isOpen();
        ContentValues values=new ContentValues();
        values.put(BD_Validation.VALIDATION_LOGIN.ID_USER,String.valueOf( LoginActivity.USUARIO.getId()));
        values.put(BD_Validation.VALIDATION_LOGIN.USER,String.valueOf(LoginActivity.USUARIO.getUser()));
        values.put(BD_Validation.VALIDATION_LOGIN.nombre,String.valueOf(LoginActivity.USUARIO.getNombre()));
        values.put(BD_Validation.VALIDATION_LOGIN.fechanac,String.valueOf(LoginActivity.USUARIO.getFechaNacimiento()));
        values.put(BD_Validation.VALIDATION_LOGIN.sexo,String.valueOf(LoginActivity.USUARIO.getSexo()));
        values.put(BD_Validation.VALIDATION_LOGIN.telefono,String.valueOf(LoginActivity.USUARIO.getTelefono()));
        values.put(BD_Validation.VALIDATION_LOGIN.telefonomov,String.valueOf(LoginActivity.USUARIO.getTelefonoMovil()));
        values.put(BD_Validation.VALIDATION_LOGIN.correo,String.valueOf(LoginActivity.USUARIO.getCorreo()));
        values.put(BD_Validation.VALIDATION_LOGIN.correoalterno,String.valueOf(LoginActivity.USUARIO.getCorreoAlterno()));
        values.put(BD_Validation.VALIDATION_LOGIN.postrgado,String.valueOf(LoginActivity.USUARIO.getPostGradoFuturo()));
        values.put(BD_Validation.VALIDATION_LOGIN.imagenPerfil,String.valueOf(LoginActivity.USUARIO.getImagenPerfil()));
        values.put(BD_Validation.VALIDATION_LOGIN.status,String.valueOf(status));

        db.update(BD_Validation.VALIDATION_LOGIN.TABLE_NAME,values,
                BD_Validation.VALIDATION_LOGIN.ID_COL+"=1",null);
        // 4. Cerramos la conexión comn la BD
        db.close();
    }
    public int existing()  {
        int exit=0;
        String query="SELECT * FROM "+ BD_Validation.VALIDATION_LOGIN.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor==null){
            exit=0;
        }else{
            if (cursor.moveToFirst()){
                exit=Integer.parseInt(cursor.getString(0));
            }
        }
        return exit;
    }
    public int getstauts(){
        int exit=0;
        String query="SELECT * FROM "+ BD_Validation.VALIDATION_LOGIN.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()){
            exit=Integer.parseInt(cursor.getString(12));
            LoginActivity.USUARIO.setId(cursor.getInt(1));
            LoginActivity.USUARIO.setUser(cursor.getString(2));
            LoginActivity.USUARIO.setNombre(cursor.getString(3));
            LoginActivity.USUARIO.setFechaNacimiento(cursor.getString(4));
            LoginActivity.USUARIO.setSexo(cursor.getInt(5));
            LoginActivity.USUARIO.setTelefono(cursor.getString(6));
            LoginActivity.USUARIO.setTelefonoMovil(cursor.getString(7));
            LoginActivity.USUARIO.setCorreo(cursor.getString(8));
            LoginActivity.USUARIO.setCorreoAlterno(cursor.getString(9));
            LoginActivity.USUARIO.setPostGradoFuturo(cursor.getString(10));
            LoginActivity.USUARIO.setImagenPerfil(cursor.getString(11));
            LoginActivity.USUARIO.setStatus(cursor.getInt(12));
        }

        return exit;
    }
}
