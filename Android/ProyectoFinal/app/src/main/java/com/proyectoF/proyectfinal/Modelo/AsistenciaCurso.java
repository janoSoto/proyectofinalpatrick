package com.proyectoF.proyectfinal.Modelo;

public class AsistenciaCurso
{
    private int id;
    private int usuario_id;
    private int curso_id;
    private int status;

    public AsistenciaCurso()
    {
        this.setId(0);
        this.setUsuario_id(0);
        this.setCurso_id(0);
        this.setStatus(0);
    }

    public AsistenciaCurso(int id, int usuario_id, int curso_id, int status)
    {
        this.setId(id);
        this.setUsuario_id(usuario_id);
        this.setCurso_id(curso_id);
        this.setStatus(status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getCurso_id() {
        return curso_id;
    }

    public void setCurso_id(int curso_id) {
        this.curso_id = curso_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
