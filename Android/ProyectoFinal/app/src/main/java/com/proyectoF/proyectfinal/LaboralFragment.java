package com.proyectoF.proyectfinal;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Laboral;
import com.proyectoF.proyectfinal.WebService.WebService;


import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.proyectoF.proyectfinal.Modelo.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;

public class LaboralFragment extends Fragment {

    private Usuario usuario = LoginActivity.USUARIO;
    private Laboral laboral = new Laboral();
    private Button bActualizar;
    private TextView tVNombre;
    private TextView tVUltimoEmpleo;
    private EditText eVPuesto;
    private EditText eVEmpresa;
    private EditText eVFecha;

    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_laboral, container, false);

        requestQueue = Volley.newRequestQueue(getContext());
        bActualizar = (Button)view.findViewById(R.id.tablaboral_button_Actualizar);
        tVNombre = (TextView) view.findViewById(R.id.tablaboral_textview_nombre);
        tVUltimoEmpleo = (TextView)view.findViewById(R.id.tablaboral_textview_ultimo);
        eVPuesto = (EditText)view.findViewById(R.id.tablaboral_editText_Puesto);
        eVEmpresa = (EditText)view.findViewById(R.id.tablaboral_editText_Empresa);
        eVFecha = (EditText)view.findViewById(R.id.tablaboral_editText_Fecha);


        setViews();

        return view;
    }

    public void setViews(){
        tVNombre.setText(usuario.getNombre());
        wsLaboralInfo();


        bActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(eVEmpresa.getText().equals("") || eVFecha.getText().equals("") || eVPuesto.getText().equals("")){
                    Toast.makeText(getContext(), "Complete los campos vacios", Toast.LENGTH_SHORT).show();
                }else{
                    wsActualizarLaboral();
                    eVEmpresa.setText("");
                    eVFecha.setText("");
                    eVPuesto.setText("");

                    wsLaboralInfo();
                }
                //Toast.makeText(getContext(), "Prueba", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void wsActualizarLaboral() {
        String url = WebService.WSLACTLABORAL +
                "?id=" + laboral.getUsuario_id() +
                "&nombreEmpresa=" +eVEmpresa.getText().toString() +
                "&puesto=" + eVPuesto.getText().toString() +
                "&fecha=" + eVFecha.getText().toString();

        url = url.replace(" ","%20");
        Log.d("actualizar info laboral",url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.optJSONArray("Laboral");
                            JSONObject jsonObject = null;
                            jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.optInt("id") > 0) {
                                Toast.makeText(getContext(), "Actualizado exitoso", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Error en el actualizar", Toast.LENGTH_SHORT).show();

                                //tVUltimoEmpleo.setText("Sin experiencia laboral");
                            }
                        } catch (Exception e) {
                            Log.d("actualizando laboral", e.getMessage());
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getContext(),"Error al actualizar",Toast.LENGTH_SHORT).show();
                        Log.d("error actualizar: ", error.getMessage());
                    }
                });
        requestQueue.add(jsonObjectRequest);
    }


    public void wsLaboralInfo() {
        String url = WebService.WSLABORAL +
                "?id=" + usuario.getId();

        url = url.replace(" ","%20");
        Log.d("pedir info laboral",url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.optJSONArray("laboral");
                            JSONObject jsonObject = null;
                            jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.optInt("id") > 0) {
                                laboral.setId(jsonObject.optInt("id"));
                                laboral.setNombreEmpresa(jsonObject.optString("nombreEmpresa"));
                                laboral.setPuesto(jsonObject.optString("puesta"));
                                laboral.setFechaIngreso(jsonObject.optString("fecha"));
                                laboral.setUsuario_id(jsonObject.optInt("id_usuario"));
                                laboral.setStatus(jsonObject.optInt("status"));


                                //tVUltimoEmpleo.setText(laboral.getPuesto() + " " + laboral.getNombreEmpresa() + " " +  laboral.getFechaIngreso());
                                tVUltimoEmpleo.setText(laboral.getPuesto() + " en " + laboral.getNombreEmpresa());

                            } else {
                                Toast.makeText(getContext(), jsonObject.optString("id"), Toast.LENGTH_SHORT).show();

                                tVUltimoEmpleo.setText("Sin experiencia laboral");
                            }
                        } catch (Exception e) {
                            Log.d("tomando info laboral", e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Ha ocurrido un error: " + error.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(jsonObjectRequest);
    }

}
