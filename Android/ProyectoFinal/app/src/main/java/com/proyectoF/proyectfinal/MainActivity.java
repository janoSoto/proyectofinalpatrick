package com.proyectoF.proyectfinal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.proyectoF.proyectfinal.Modelo.SQLite.bd_validationQuery;

public class MainActivity extends Activity
{
    private final int DURACION_SPLASH = 2000;
    Intent intent=null;
    bd_validationQuery bd_validationQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        bd_validationQuery=new bd_validationQuery(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                chek();

            };
        }, DURACION_SPLASH);

    }
    public boolean checkinternt(){
        boolean exit=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d("MIAPP", "Estás online");

            Log.d("MIAPP", " Estado actual: " + networkInfo.getState());

            exit=true;
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // Estas conectado a un Wi-Fi
                Log.d("MIAPP", " Nombre red Wi-Fi: " + networkInfo.getExtraInfo());
            }

        } else {
            exit=false;
            Log.d("MIAPP", "Estás offline");
        }
        Log.d("MIAPP", ""+exit);
        return exit;
    }
    public void chek(){
        if (checkinternt()==true){
            new BackgroundSplashTask().execute();
        }else{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Conexión a internet fallida.");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Reintentar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chek();

                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }
    private class BackgroundSplashTask extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Object doInBackground(Object[] objects) {
            int a=0,b=0;
            try {
                a=bd_validationQuery.existing();
            }catch (Exception e){
                Log.e("Error listado3",""+e);
            }


            if (a == 0) {
                try {
                    bd_validationQuery.insert();
                } catch (Exception e) {
                    Log.e("Error listado1", "" + e);
                }
                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();


            } else {
                try {
                    b = bd_validationQuery.getstauts();
                } catch (Exception e) {
                }
                if (b == 0) {
                    intent = new Intent(MainActivity.this, LoginActivity.class);
                    // startActivity(intent);
                    // finish();
                } else {
                    intent = new Intent(MainActivity.this, Menu.class);
                    // startActivity(intent);


                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {

            startActivity(intent);
            finish();


        }
    }
    private void onNetworkChange(NetworkInfo networkInfo) {
        if (networkInfo != null) {
            if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                Log.d("MenuActivity", "CONNECTED");

            } else {
                chek();
                Log.d("MenuActivity", "DISCONNECTED");
            }
        }
    }
    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            onNetworkChange(ni);
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        unregisterReceiver(networkStateReceiver);
        super.onPause();
    }


}
