package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Cursos;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener
{
    private final Context context = this;
    private ProcesosPHPCursos php = new ProcesosPHPCursos();
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String URL= "https://proyectofinalp.000webhostapp.com/WS/";
    private Button btnIr;

    private ArrayList<Cursos> listaCursos = new ArrayList<Cursos>();
    ListView list;
    ListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        list = (ListView) findViewById(R.id.list);


        if(isOnline(getApplicationContext()))
        {
            try
            {
                request = Volley.newRequestQueue(context);
                php.setContext(Main2Activity.this);
                consultarTodosWs();
            }catch (Exception e)
            {
                enviarMensaje("Error en consultar Todos: " + e.getMessage());
            };
        }
        else
        {
            enviarMensaje("Su dispositvo no cuenta con conexión a internet");
        }
    }

    //metodo Consultar Todos
    private void consultarTodosWs()
    {
        try{
            String url= URL + "wsCursos.php";
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);
        }catch(Exception e)
        {
            Toast.makeText(getApplicationContext(),"error en consultar todos función: " + e.getMessage(),Toast.LENGTH_SHORT).show();
        };
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        error.printStackTrace();
        Toast.makeText(getApplicationContext(),"error en el volley: "+ error.getMessage(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResponse(JSONObject response)
    {
        Cursos estado = null;
        try
        {
            JSONArray json = response.optJSONArray("cursos");
            try{
                for(int i=0;i<json.length();i++)
                {
                    estado = new Cursos();
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(i);
                    estado.setId(jsonObject.optInt("id"));
                    estado.setNombreCurso(jsonObject.optString("nombre"));
                    estado.setNombreProfesor(jsonObject.optString("profesor"));
                    estado.setFecha(jsonObject.optString("fecha"));
                    estado.setHora(jsonObject.optString("hora"));
                    listaCursos.add(estado);
                }
            }catch (Exception e)
            {
                enviarMensaje("Error try 2 ON RESPONSE: " + e.getMessage());
            };

            adapter = new ListViewAdapter(this, listaCursos);

            // Binds the Adapter to the ListView
            list.setAdapter(adapter);

        }catch(Exception e)
        {
            enviarMensaje("Error try 1 ON RESPONSE: " + e.getMessage());
        };

    }

    public static boolean isOnline(Context context)
    {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public void enviarMensaje(String msj)
    {
        Toast.makeText(getApplicationContext(),msj, Toast.LENGTH_SHORT).show();
    }
}
