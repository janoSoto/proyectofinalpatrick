package com.proyectoF.proyectfinal;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProcesosPHPCursos implements Response.Listener<JSONObject>, Response.ErrorListener
{
    private RequestQueue request;
    private int res;
    private JsonObjectRequest jsonObjectRequest;
    private int idI;
    private String serverip = "https://proyectofinalp.000webhostapp.com/WS/";

    public void setContext(Context context)
    {
        request = Volley.newRequestQueue(context);
    }

    public void insertarAsistencia(final int idUsuario, final int idCurso, final int estatus)
    {
        idI = 0;
        String url = serverip + "wsRegistroAsistencia.php?usuario="+ idUsuario
                +"&curso="+ idCurso+"&estatus="+ estatus;
        url = url.replace(" ","%20");
        Log.e("ghhjg",""+url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {

                JSONArray json = response.optJSONArray("resul");
                try
                {
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(0);
                    idI = jsonObject.optInt("id");
                    Log.e("ID ID ID",""+ idI);
                }
                catch (Exception e)
                {
                    Log.e("ERRORRRORORORORO:     ",""+ e.getMessage());
                }
            }
        }, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

    }
}
