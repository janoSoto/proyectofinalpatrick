package com.proyectoF.proyectfinal.WebService;

public class WebService {
    public static String WSACTUALIZAR = "https://proyectofinalp.000webhostapp.com/WS/actualizar.php";
    public static String WSLOGIN = "https://proyectofinalp.000webhostapp.com/WS/login.php";
    public static String WSLABORAL = "https://proyectofinalp.000webhostapp.com/WS/laboral.php";
    public static String WSPOSTGRADO = "https://proyectofinalp.000webhostapp.com/WS/ActualizarPost.php";
    public static String WSLACTLABORAL = "https://proyectofinalp.000webhostapp.com/WS/actualizarLaboral.php";
}
