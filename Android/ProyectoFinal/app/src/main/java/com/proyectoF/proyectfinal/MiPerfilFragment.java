package com.proyectoF.proyectfinal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.proyectoF.proyectfinal.Modelo.Usuario;
import com.squareup.picasso.Picasso;

public class MiPerfilFragment extends Fragment {
    private Usuario usuario = LoginActivity.USUARIO;
    private ImageView iVPerfil;
    private TextView tVNombre;
    private TextView tVCarrera;
    private TextView tVFechaNacimiento;
    private TextView tVSexo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_mi_perfil, container, false);

        iVPerfil = (ImageView) view.findViewById(R.id.tabmiperfil_imageview_foto);
        tVNombre = (TextView) view.findViewById(R.id.tabmiperfil_textview_nombre);
        tVCarrera = (TextView) view.findViewById(R.id.tabmiperfil_textview_carrera);
        tVFechaNacimiento = (TextView) view.findViewById(R.id.tabmiperfil_textview_fecha);
        tVSexo = (TextView) view.findViewById(R.id.tabmiperfil_textview_sexo);

        setViews();
        return view;
    }

    public void setViews() {
        try{
            Picasso.with(getContext()).load(usuario.getImagenPerfil()).error(R.drawable.ic_launcher_background).fit().centerInside().into(iVPerfil);
        }catch (Exception e) {
            e.getStackTrace();
        }
        tVNombre.setText(usuario.getNombre());
        tVCarrera.setText(usuario.getCarrera());
        tVFechaNacimiento.setText(usuario.getFechaNacimiento());
        if(usuario.getSexo()==1) {
            tVSexo.setText("Masculino");
        }else if(usuario.getSexo()==2) {
            tVSexo.setText("Femenino");
        }
    }
}