package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.proyectoF.proyectfinal.Modelo.SQLite.bd_validationQuery;

public class Menu extends AppCompatActivity
{
    private LinearLayout linePerfil, lineActividad, lineCursos, lineVacantes;

    bd_validationQuery bd_validationQuery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        lineActividad = (LinearLayout) findViewById(R.id.lineActividad);
        lineCursos = (LinearLayout) findViewById(R.id.lineCursos);
        linePerfil = (LinearLayout) findViewById(R.id.linePerfil);
        lineVacantes = (LinearLayout) findViewById(R.id.lineVacantes);

        bd_validationQuery = new bd_validationQuery(this);

        lineVacantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "CLICK VACANTES", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), Vacantes.class);
                startActivityForResult(intent, 0);
            }
        });

        linePerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu.this,PerfilActivity.class);
                startActivity(i);
            }
        });
        lineCursos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Main2Activity.class);
                startActivityForResult(intent, 0);
            }
        });
        lineActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "CLICK ACTIVIDAD", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), ActividadActualActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.m_login)
        {

            bd_validationQuery.update(0,0);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
