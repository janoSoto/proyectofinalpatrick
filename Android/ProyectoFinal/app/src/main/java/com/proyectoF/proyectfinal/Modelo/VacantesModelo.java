package com.proyectoF.proyectfinal.Modelo;

import java.io.Serializable;

public class VacantesModelo implements Serializable
{
    private int id;
    private String nombreEmpresa;
    private String requisitos;
    private String nombreVacante;
    private int status;

    public VacantesModelo()
    {
        this.id = 0;
        this.nombreEmpresa = "";
        this.nombreVacante = "";
        this.requisitos = "";
        this.status = 0;
    }

    public VacantesModelo(int id, String nombreEmpresa, String requisitos, String nombreVacante, int status) {
        this.id = id;
        this.nombreEmpresa = nombreEmpresa;
        this.requisitos = requisitos;
        this.nombreVacante = nombreVacante;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public String getNombreVacante() {
        return nombreVacante;
    }

    public void setNombreVacante(String nombreVacante) {
        this.nombreVacante = nombreVacante;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
