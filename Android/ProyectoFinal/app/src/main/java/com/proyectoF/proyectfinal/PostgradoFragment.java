package com.proyectoF.proyectfinal;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Usuario;
import com.proyectoF.proyectfinal.WebService.WebService;

import org.json.JSONObject;


public class PostgradoFragment extends Fragment implements Response.Listener<JSONObject> {

    private Usuario usuario = LoginActivity.USUARIO;
    private Postgrado postgrado = new Postgrado();
    private TextView tVNombre;
    private EditText eTNombrePostgrado;
    private EditText eTNombreEscuela;
    private Button BtActualizarEmpleo;
    private RadioButton rBSi;
    private RadioButton rBNo;
    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab_postgrado, container, false);
        requestQueue = Volley.newRequestQueue(getContext());

        BtActualizarEmpleo = (Button) view.findViewById(R.id.Postgrado_btn_ActualizarEmpleado);
        tVNombre = (TextView) view.findViewById(R.id.Postgrado_textView_Nombre);
        eTNombrePostgrado = (EditText) view.findViewById(R.id.Postgrado_EditTxt_NombrePostgrado);
        eTNombreEscuela = (EditText) view.findViewById(R.id.Postgrado_EditTxt_Escuela);
        rBSi = (RadioButton) view.findViewById(R.id.Postgrado_rbtn_Si);
        rBNo = (RadioButton) view.findViewById(R.id.Postgrado_rbtn_No);

        BtActualizarEmpleo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(eTNombrePostgrado.getText().equals("") || eTNombreEscuela.getText().equals("") ){
                    Toast.makeText(getContext(), "Complete los campos vacios", Toast.LENGTH_SHORT).show();
                }else{
                    wsActualizar();
                }
            }
        });

        setViews();

        return view;

    }


    public void setViews(){
        tVNombre.setText(usuario.getNombre());
        int i=Integer.parseInt(usuario.getPostGradoFuturo());
        if (i>0){
            rBSi.isSelected();
        }else {
            rBNo.isSelected();
        }
    }

    public void wsActualizar() {
        int status = 0;
        if(rBSi.isSelected()) {
            status = 1;
        }

        String url = WebService.WSPOSTGRADO +
                "?id=" + usuario.getId() +
                "&nombrePostgrado=" + eTNombrePostgrado.getText().toString() +
                "&nombreEscuela=" + eTNombreEscuela.getText().toString() +
                "&status=" + status;

        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,this,null);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getContext(),"Datos Actualizados",Toast.LENGTH_SHORT).show();
    }
}
