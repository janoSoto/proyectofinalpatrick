package com.proyectoF.proyectfinal.Modelo;

public class Postgrado
{
    private int id;
    private String nombrePostGrado;
    private String nombreEscuela;
    private int usuario_id;
    private int status;

    public Postgrado()
    {
        this.setId(0);
        this.setNombrePostGrado("");
        this.setNombreEscuela("");
        this.setUsuario_id(0);
        this.status = 0;
    }

    public Postgrado(int id, String nombrePostGrado, String nombreEscuela, int usuario_id, int status)
    {
        this.setId(id);
        this.setNombrePostGrado(nombrePostGrado);
        this.setNombreEscuela(nombreEscuela);
        this.setUsuario_id(usuario_id);
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrePostGrado() {
        return nombrePostGrado;
    }

    public void setNombrePostGrado(String nombrePostGrado) {
        this.nombrePostGrado = nombrePostGrado;
    }

    public String getNombreEscuela() {
        return nombreEscuela;
    }

    public void setNombreEscuela(String nombreEscuela) {
        this.nombreEscuela = nombreEscuela;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }
}
