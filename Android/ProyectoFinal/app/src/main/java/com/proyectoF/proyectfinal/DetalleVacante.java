package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.proyectoF.proyectfinal.Modelo.VacantesModelo;

public class DetalleVacante extends AppCompatActivity {

    private VacantesModelo curso;
    private TextView txtNombre, txtProfesor, txtFecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_vacante);

        txtNombre = (TextView) findViewById(R.id.txtNombreVacanteD);
        txtFecha = (TextView) findViewById(R.id.txtRequisitos);
        txtProfesor = (TextView) findViewById(R.id.txtNombreEmpresaD);

        try
        {
            Bundle oBundle = getIntent().getExtras();
            VacantesModelo cur = (VacantesModelo) oBundle.getSerializable("curso");
            curso = cur;

            txtProfesor.setText(curso.getNombreEmpresa());
            txtNombre.setText(curso.getNombreVacante());
            txtFecha.setText(curso.getRequisitos());


        }
        catch (Exception e)
        {
            enviarMensaje("Error en cargar bundle: " + e.getMessage());
        }
    }

    public void enviarMensaje(String msj)
    {
        Toast.makeText(getApplicationContext(),msj, Toast.LENGTH_SHORT).show();
    }
}
