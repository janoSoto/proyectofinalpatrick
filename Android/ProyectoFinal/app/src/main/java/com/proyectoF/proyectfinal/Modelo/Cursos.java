package com.proyectoF.proyectfinal.Modelo;

import java.io.Serializable;

public class Cursos implements Serializable
{
    private int id;
    private String nombreProfesor;
    private String fecha;
    private String hora;
    private String nombreCurso;
    private int status;

    public Cursos()
    {
        this.id = 0;
        this.nombreProfesor = "";
        this.fecha = "";
        this.hora = "";
        this.nombreCurso = "";
        this.status = 0;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}