package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Cursos;

import org.json.JSONArray;
import org.json.JSONObject;

public class DetalleCurso extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener
{
    private int ID_USER = 1;
    private int STATUS = -1;
    private int ID_CURSO = 0;
    private Cursos curso;
    private TextView txtNombre, txtProfesor, txtFecha;
    private RadioButton rd1, rd2;
    private RadioGroup rG;
    private Button btnEnviar;
    private ProcesosPHPCursos php = new ProcesosPHPCursos();
    private String URL= "https://proyectofinalp.000webhostapp.com/WS/";
    private JsonObjectRequest jsonObjectRequest;


    private final Context context = this;
    private RequestQueue request;

    private TextView txtInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_curso);

        php.setContext(this);
        txtNombre = (TextView) findViewById(R.id.txtNombreCursoD);
        txtFecha = (TextView) findViewById(R.id.txtFecha);
        txtInfo = (TextView) findViewById(R.id.txtInfo);
        txtProfesor = (TextView) findViewById(R.id.txtNombreProfesorD);
        rd1 = (RadioButton) findViewById(R.id.rd1);
        rd2 = (RadioButton) findViewById(R.id.rd2);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        rG = (RadioGroup) findViewById(R.id.rG);

        try
        {
            Bundle oBundle = getIntent().getExtras();
            Cursos cur = (Cursos) oBundle.getSerializable("curso");
            curso = cur;

            txtProfesor.setText(curso.getNombreProfesor());
            txtNombre.setText(curso.getNombreCurso());
            txtFecha.setText(curso.getFecha()+" " + curso.getHora());
            ID_CURSO = curso.getId();

            try
            {
                request = Volley.newRequestQueue(context);
                php.setContext(DetalleCurso.this);
                consultarTodosWs();
            }catch (Exception e)
            {
                enviarMensaje("Error en consultar Todos: " + e.getMessage());
            };


        }
        catch (Exception e)
        {
            enviarMensaje("Error en cargar bundle: " + e.getMessage());
        }

        btnEnviar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(rd1.isChecked()== true )
                {
                    dialogInsertar("SI");
                    STATUS = 1;

                }
                else if(rd2.isChecked()== true)
                {
                    dialogInsertar("NO");
                    STATUS = 2;
                }
                else
                {
                    enviarMensaje("Favor Seleccionar alguna opción de asistencia");
                }
            }
        });
    }

    public void guardarSQL()
    {

        try
        {
            php.insertarAsistencia(ID_USER, ID_CURSO, STATUS);
            enviarMensaje("Se envió con exito su petición");
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Error en box Guardar: " + e.getMessage(),Toast.LENGTH_SHORT).show();
        }

    }

    //metodo Consultar Todos
    private void consultarTodosWs()
    {
        try{
            //String url= URL + "wsCursos.php";
            String url = URL + "wsConsultarAsistencia.php?id="+ ID_USER
                    +"&idcurso="+ ID_CURSO;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);
        }catch(Exception e)
        {
            Toast.makeText(getApplicationContext(),"error en consultar todos función: " + e.getMessage(),Toast.LENGTH_SHORT).show();
        };
    }


    public void enviarMensaje(String msj)
    {
        Toast.makeText(getApplicationContext(),msj, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorResponse(VolleyError error)
    {
        error.printStackTrace();
        Toast.makeText(getApplicationContext(),"error en el volley: "+ error.getMessage(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResponse(JSONObject response)
    {
        try
        {
            JSONArray json = response.optJSONArray("cursos");
            try{
                for(int i=0;i<json.length();i++)
                {
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(i);
                    int asis = jsonObject.optInt("id");
                    if(asis == 0)
                    {
                        STATUS = 0;
                    }
                    else if(asis == -1)
                    {
                        Toast.makeText(getApplicationContext(),"error en on response\n Error de Conexión",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        int esta = jsonObject.optInt("status");
                        STATUS = esta;
                        if(STATUS == 1)
                        {
                            rd1.setChecked(true);
                            rd2.setChecked(false);
                            txtInfo.setText("Usted Confirmo su Asistencia");
                        }
                        if(STATUS == 2)
                        {
                            rd2.setChecked(true);
                            rd1.setChecked(false);
                            txtInfo.setText("Usted confirmo su Inasistencia");
                        }
                        btnEnviar.setEnabled(false);
                        rd1.setEnabled(false);
                        rd2.setEnabled(false);
                    }

                }
            }catch (Exception e)
            {
                enviarMensaje("Error try 2 ON RESPONSE: " + e.getMessage());
            };

        }catch(Exception e)
        {
            enviarMensaje("Error try 1 ON RESPONSE: " + e.getMessage());
        };

    }

    public void dialogInsertar(String res)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("¿Seguro que desea " + res +" asistir a la conferencia?");
        alertDialogBuilder.setPositiveButton("Si",
                new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {


                        guardarSQL();
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        Toast.makeText(getApplicationContext(),"bay", Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
