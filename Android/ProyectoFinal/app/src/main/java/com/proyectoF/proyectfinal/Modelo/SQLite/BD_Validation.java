package com.proyectoF.proyectfinal.Modelo.SQLite;

public class BD_Validation {

    public static final String DATABASE_NAME = "Validation_login";
    //Version de la Base de Datos (Este parámetro es importante  )
    public static final int DATABASE_VERSION = 3;

    //Una clase estatica en la que se definen las propiedaes que determinan la tabla Notes
    // y sus 4 columnas
    public static class VALIDATION_LOGIN {
        //Nombre de la tabla
        public static final String TABLE_NAME = "validation_log";
        //Nombre de las Columnas que contiene la tabla
        public static final String ID_COL = "id";
        public static final String ID_USER = "id_user";
        public static final String USER = "user";
        public static final String nombre = "nombre";
        public static final String fechanac = "fechanac";
        public static final String sexo = "sexo";
        public static final String telefono = "telefono";
        public static final String telefonomov = "telefonomov";
        public static final String correo = "correo";
        public static final String correoalterno = "correoalterno";
        public static final String postrgado = "postrgado";
        public static final String imagenPerfil = "imagenPerfil";
        public static final String status = "status";
    }

    //Setencia SQL que permite crear la tabla Notes
    public static final String LOGIN_TABLE_CREATE =
            "CREATE TABLE " + VALIDATION_LOGIN.TABLE_NAME + "( " +
                    VALIDATION_LOGIN.ID_COL +" INTEGER ,"+
                    VALIDATION_LOGIN.ID_USER +" INTEGER ,"+
                    VALIDATION_LOGIN.USER +" TEXT ,"+
                    VALIDATION_LOGIN.nombre +" TEXT ,"+
                    VALIDATION_LOGIN.fechanac +" TEXT ,"+
                    VALIDATION_LOGIN.sexo +" INTEGER ,"+
                    VALIDATION_LOGIN.telefono +" TEXT ,"+
                    VALIDATION_LOGIN.telefonomov +" TEXT ,"+
                    VALIDATION_LOGIN.correo +" TEXT ,"+
                    VALIDATION_LOGIN.correoalterno +" TEXT ,"+
                    VALIDATION_LOGIN.postrgado +" TEXT ,"+
                    VALIDATION_LOGIN.imagenPerfil +" TEXT ,"+
                    VALIDATION_LOGIN.status + " INTEGER );";

    //Setencia SQL que permite eliminar la tabla Notes
    public static final String LOGIN_TABLE_DROP = "DROP TABLE IF EXISTS " + VALIDATION_LOGIN.TABLE_NAME;


}
