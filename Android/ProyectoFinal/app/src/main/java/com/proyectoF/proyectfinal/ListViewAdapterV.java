package com.proyectoF.proyectfinal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.proyectoF.proyectfinal.Modelo.VacantesModelo;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapterV extends BaseAdapter
{
    Context mContext;
    LayoutInflater inflater;
    private List<VacantesModelo> animalNamesList = null;
    private ArrayList<VacantesModelo> arraylist;
    private ProcesosPHPCursos php = new ProcesosPHPCursos();;
    int total = 0;


    public ListViewAdapterV(Context context, ArrayList<VacantesModelo> animalNamesList) {
        mContext = context;
        this.animalNamesList = animalNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<VacantesModelo>();
        this.arraylist.addAll(animalNamesList);
        total = animalNamesList.size();
    }

    public class ViewHolder
    {
        TextView name;
        TextView nameP;
        LinearLayout lineCurso;
    }

    @Override
    public int getCount() {
        return animalNamesList.size();
    }

    @Override
    public Object getItem(int i) {
        return animalNamesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup)
    {
        final ViewHolder holder;
        if (view == null)
        {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.layout_curso, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.lblNombreCurso);
            holder.nameP = (TextView) view.findViewById(R.id.lblNombreProfesor);
            holder.lineCurso = (LinearLayout) view.findViewById(R.id.lineCurso);
            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }

        holder.name.setText(animalNamesList.get(i).getNombreVacante());
        holder.nameP.setText(animalNamesList.get(i).getNombreEmpresa());


        holder.lineCurso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {
                    Toast.makeText(mContext, "CLICKEASTE: " + animalNamesList.get(i).getNombreVacante(),Toast.LENGTH_SHORT).show();

                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("curso", animalNamesList.get(i));
                   Intent intent = new Intent(mContext,DetalleVacante.class);
                    intent.putExtras(oBundle);
                    mContext.startActivity(intent);
                }catch (Exception e)
                {
                    Toast.makeText(mContext, e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}
