package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.Hash;
import com.proyectoF.proyectfinal.Modelo.SQLite.bd_validationQuery;
import com.proyectoF.proyectfinal.Modelo.Usuario;
import com.proyectoF.proyectfinal.WebService.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject> {
    public static Usuario USUARIO = new Usuario();
    private EditText txtUser;
    private EditText txtPass;
    private Button btnIniciar;
    private Button btnRegistrar;
    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;
    bd_validationQuery bd_validationQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        requestQueue = Volley.newRequestQueue(this);
        getSupportActionBar().hide();
        txtPass= (EditText) findViewById(R.id.txtPass);
        txtUser= (EditText) findViewById(R.id.txtUser);
        btnIniciar= (Button) findViewById(R.id.btnIniciar);
        btnRegistrar= (Button) findViewById(R.id.btnRegistrar);
        bd_validationQuery = new bd_validationQuery(this);

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                if (txtPass.getText().toString().matches("") && txtUser.getText().toString().matches("")) {
                    txtPass.setError("Ingrese datos");
                    txtUser.setError("Ingrese datos");
                } else {
                    wsIniciarSesion(txtUser.getText().toString(),txtPass.getText().toString());
                }
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(getApplicationContext(),"Acuda al departamento de sistemas para proporcionarle sus datos",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void wsIniciarSesion(String usuario, String contrasenia) {
        String url = WebService.WSLOGIN +
                "?usuario=" + usuario +
                "&password=" + Hash.md5(contrasenia);

        url = url.replace(" ","%20");
        Log.d("prueba123",url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this,"error al autentificar usuario y/0 contraseñña incorrecto ",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        try{
            JSONArray jsonArray = response.optJSONArray("Usuario");
            JSONObject jsonObject = null;
            jsonObject = jsonArray.getJSONObject(0);

            if(jsonObject.optInt("id")>0) {
                USUARIO.setId(jsonObject.optInt("id"));
                USUARIO.setUser(jsonObject.optString("user"));
                USUARIO.setPassword(jsonObject.optString("password"));
                USUARIO.setNombre(jsonObject.optString("nombre"));
                USUARIO.setFechaNacimiento(jsonObject.optString("fechaNacimiento"));
                USUARIO.setSexo(jsonObject.optInt("sexo"));
                USUARIO.setTelefono(jsonObject.optString("telefono"));
                USUARIO.setTelefonoMovil(jsonObject.optString("telefonomovil"));
                USUARIO.setCorreo(jsonObject.optString("correo"));
                USUARIO.setCorreoAlterno(jsonObject.optString("correoAlterno"));
                USUARIO.setPostGradoFuturo(jsonObject.optString("postgradoFuturo"));
                USUARIO.setImagenPerfil(jsonObject.optString("imagenPerfil"));
                USUARIO.setStatus(jsonObject.optInt("status"));
                USUARIO.setCarrera(jsonObject.optString("nombreCarrera"));
                bd_validationQuery.update(1,USUARIO.getId());

                Intent intent = new Intent(LoginActivity.this, Menu.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(this,jsonObject.optString("usuario"),Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e) {
            Log.d("prueba123",e.getMessage());
            e.printStackTrace();
        }
    }
}
