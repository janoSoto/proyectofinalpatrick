package com.proyectoF.proyectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.proyectoF.proyectfinal.Modelo.VacantesModelo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Vacantes extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener{

    private final Context context = this;
    private ProcesosPHPCursos php = new ProcesosPHPCursos();
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String URL="https://proyectofinalp.000webhostapp.com/WS/";


    private ArrayList<VacantesModelo> listaVacantes = new ArrayList<VacantesModelo>();
    ListView list;
    ListViewAdapterV adapter;
    private Button btnIr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantes);

        list = (ListView) findViewById(R.id.listV);

        if(isOnline(getApplicationContext()))
        {
            try
            {
                request = Volley.newRequestQueue(context);
                php.setContext(Vacantes.this);
                consultarTodosWs();
            }catch (Exception e)
            {
                enviarMensaje("Error en consultar Todos: " + e.getMessage());
            };
        }
        else
        {
            enviarMensaje("Su dispositvo no cuenta con conexión a internet");
        }


    }

    private void consultarTodosWs()
    {
        try{
            String url= URL + "wsConsultarVacantes.php";
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);
        }catch(Exception e)
        {
            Toast.makeText(getApplicationContext(),"error en consultar todos función: " + e.getMessage(),Toast.LENGTH_SHORT).show();
        };
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        Toast.makeText(getApplicationContext(),"error en el volley: "+ error.getMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {

        VacantesModelo estado = null;
        try
        {
            JSONArray json = response.optJSONArray("vacantes");
            try{
                for(int i=0;i<json.length();i++)
                {
                    estado = new VacantesModelo();
                    JSONObject jsonObject = null;
                    jsonObject=json.getJSONObject(i);
                    estado.setId(jsonObject.optInt("id"));
                    estado.setNombreVacante(jsonObject.optString("nombre"));
                    estado.setNombreEmpresa(jsonObject.optString("empresa"));
                    estado.setRequisitos(jsonObject.optString("requisitos"));
                    listaVacantes.add(estado);
                }
            }catch (Exception e)
            {
                enviarMensaje("Error try 2 ON RESPONSE: " + e.getMessage());
            };

            adapter = new ListViewAdapterV(this,listaVacantes);

            // Binds the Adapter to the ListView
            list.setAdapter(adapter);

        }catch(Exception e)
        {
            enviarMensaje("Error try 1 ON RESPONSE: " + e.getMessage());
        };

    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public void enviarMensaje(String msj)
    {
        Toast.makeText(getApplicationContext(),msj, Toast.LENGTH_SHORT).show();
    }
}
