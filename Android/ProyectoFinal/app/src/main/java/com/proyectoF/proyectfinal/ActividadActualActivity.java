package com.proyectoF.proyectfinal;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class ActividadActualActivity extends FragmentActivity {

    private FragmentTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_actual);
        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab_laboral").setIndicator("Laboral"), LaboralFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab_postgrado").setIndicator("Postgrado"), PostgradoFragment.class, null);
    }

}
