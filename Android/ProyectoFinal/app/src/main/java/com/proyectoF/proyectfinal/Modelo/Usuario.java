package com.proyectoF.proyectfinal.Modelo;

public class Usuario {
    private int id;
    private String user;
    private String password;
    private String nombre;
    private String fechaNacimiento;
    private int sexo;
    private String telefono;
    private String telefonoMovil;
    private String correo;
    private String correoAlterno;
    private String postGradoFuturo;
    private String imagenPerfil;
    private int status;
    private String carrera;

    public Usuario() {
        this.setId(0);
        this.setUser("");
        this.setPassword("");
        this.setNombre("");
        this.setFechaNacimiento("");
        this.setSexo(0);
        this.setTelefono("");
        this.setTelefonoMovil("");
        this.setCorreo("");
        this.setCorreoAlterno("");
        this.setImagenPerfil("");
        this.setStatus(0);
    }

    public Usuario(int id, String user, String password, String nombre, String fechaNacimiento, int sexo, String telefono, String telefonoMovil, String correo, String correoAlterno, String postGradoFuturo, String imagenPerfil, int status) {
        this.setId(id);
        this.setUser(user);
        this.setPassword(password);
        this.setNombre(nombre);
        this.setFechaNacimiento(fechaNacimiento);
        this.setSexo(sexo);
        this.setTelefono(telefono);
        this.setTelefonoMovil(telefonoMovil);
        this.setCorreo(correo);
        this.setCorreoAlterno(correoAlterno);
        this.setImagenPerfil(imagenPerfil);
        this.setStatus(status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoAlterno() {
        return correoAlterno;
    }

    public void setCorreoAlterno(String correoAlterno) {
        this.correoAlterno = correoAlterno;
    }

    public String getPostGradoFuturo() {
        return postGradoFuturo;
    }

    public void setPostGradoFuturo(String postGradoFuturo) {
        this.postGradoFuturo = postGradoFuturo;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getImagenPerfil() {
        return imagenPerfil;
    }

    public void setImagenPerfil(String imagenPerfil) {
        this.imagenPerfil = imagenPerfil;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
