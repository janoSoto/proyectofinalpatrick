package com.proyectoF.proyectfinal;


import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class PerfilActivity extends FragmentActivity {

    private FragmentTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab_mi_perfil").setIndicator("Mi Perfil"), MiPerfilFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab_contacto").setIndicator("Contacto"), ContactoActivity.class, null);
    }
}
